// load json file function using native js
var loadJSON = function(path, success, error) {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (success && xhr.status === 200) {
                success(JSON.parse(xhr.responseText));
            } else if (error) {
                error(xhr);
            }
        }
    };
    xhr.open("GET", path, true);
    xhr.send();
};
