// Lunch component
Vue.component('lunch', {
    data: function () {
        return {
            resultsReady: false,

            ingredients: null,
            recipes: null,
            possibleRecipeCount: 0,
            today: new Date(),
        }
    },
    methods: {
        // load recipies and ingredients
        loadRecipesIngredients: function(callback) {
            let instance = this;

            // first promise loads ingredients
            let promiseIngredients = new Promise((resolve, reject) => {
                loadJSON('./data/ingredients.json',
                    function(data) {
                        instance.ingredients = data.ingredients;
                        resolve(true);
                    }, function(xhr) {
                        console.error(xhr);
                        reject();
                    }
                );
            });

            // second promise loads recipes
            let promiseRecipes = new Promise((resolve, reject) => {
                loadJSON('./data/recipes.json',
                    function(data) {
                        instance.recipes = data.recipes;
                        resolve(true);
                    }, function(xhr) {
                        console.error(xhr);
                        reject();
                    }
                );
            });

            // after we load both json files - back to main workflow
            Promise.all([promiseIngredients, promiseRecipes]).then(function() {
                callback();
            });
        },
        whatsForLunch: function (event) {
            let instance = this;

            this.loadRecipesIngredients(function() {

                // filter all ingredients which we can not use (not fresh)
                instance.ingredients = instance.ingredients.filter((ingredient) => {
                    return new Date(ingredient['use-by']) > instance.today;
                });

                // ingredients titles only
                let ingredientsTitles = instance.ingredients.map((ingredient) => ingredient.title);

                // define receipts which we can cook (we have all ingredients), and set min bestBefore date
                instance.recipes.map((recipe) => {
                    recipe.canCook = recipe.ingredients.every(ingredient => ingredientsTitles.includes(ingredient));
                    if (recipe.canCook) {
                        instance.possibleRecipeCount++;
                    }
                    recipe.ingredients.every(ingredient => {
                        let ingredientInfo = instance.ingredients.find(product => {return product.title == ingredient;} );
                        let productDate = new Date(ingredientInfo['best-before']);
                        recipe.bestBefore = (recipe.bestBefore && recipe.bestBefore > productDate) ? recipe.bestBefore : productDate;
                    });
                });

                // sort by the best before
                instance.recipes.sort(function(a, b) {
                    return a.bestBefore > b.bestBefore;
                });

                instance.resultsReady = true;
            });
        }
    }
});

// main Vue application
var cookApp = new Vue({
    el: '.main'
});