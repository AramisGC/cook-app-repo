// Import Vue and lunch component
import Vue from 'vue'
import lunch from './vue/lunch.vue'

describe('lunch', () => {
  it('lunch component is created', () => {
    expect(typeof lunch.created).toBe('function')
  })

  it('check for default data (resultsReady field)', () => {
    expect(typeof lunch.data).toBe('function')
    const defaultData = lunch.data()
    expect(defaultData.resultsReady).toBe(false)
  })

})