INSTALLATION

    1) prepare a server (apache / windows iis server)

    2) upload the following files onto the server:

        data\ingredients.json
        data\recipes.json
        js\app.js
        js\util.js
        index.html

    3) open index.html in browser on your server


TESTING

    Initial unit test is in provided test.js
    (incomplete / in process)


USAGE INSTRUCTION

    1) open the index.html page

    2) click "What’s for lunch" button

    3) all possible receipts should appear